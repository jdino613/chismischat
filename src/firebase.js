import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import "firebase/storage";

var firebaseConfig = {
  apiKey: "AIzaSyBqNBofu3XPSUteYy16DPVed3mw8FXAtZw",
  authDomain: "chismis-4fd85.firebaseapp.com",
  databaseURL: "https://chismis-4fd85.firebaseio.com",
  projectId: "chismis-4fd85",
  storageBucket: "chismis-4fd85.appspot.com",
  messagingSenderId: "893732764917",
  appId: "1:893732764917:web:7bdc6cf12b3fee794c19bc",
  measurementId: "G-NRZFWMZVKX",
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// firebase.analytics();

export default firebase;
